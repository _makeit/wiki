# Intro to accounting and double-entry bookkeeping with ledger-cli
[ledger-cli](https://www.ledger-cli.org) is a free and open source program for double-entry accounting. It uses a [plain-text accounting](https://plaintextaccounting.org/) style where each transaction entry is written in a simple text format in a document. Following the style format specified, we can then use the program to automatically generate various reports and perform calculations on our transactions.
In this primer we will show a few examples of how to use ledger-cli. More information can be found on these links:
- [ledger-cli documentation](https://www.ledger-cli.org/3.0/doc/ledger3.html)
- [ledger-cli cheatsheet](https://devhints.io/ledger)
- [ledger-cli article1](https://sachachua.com/blog/tag/ledger/#post-27575)
- [ledger-cli article2](https://frdmtoplay.com/envelope-budgeting-with-ledger/)


## Getting and installing ledger-cli on Windows
Ledger-cli can be obtained in a few quick steps:

1. Download the ledger-cli binary or zip file https://www.ledger-cli.org/download.html
1. Extract the zip file so you only have the ledger executable program file
1. Place the ledger program file in the same folder as your document
1. Open the command terminal by clicking on an empty part of the Address Bar of File Explorer and then type `cmd` and press Enter
1. Generate a balance report of your document with the command `ledger -f YOUR_DOCUMENT balance`


## The document: Adding transactions to your ledger
This is how a simple uncleared transaction looks. The income of our sale is 120 which then becomes our assets. In double-entry accounting we ordinarily have to type the amount on both accounts everytime we make an entry, but when we leave 1 account blank as below, then the program will automatically calculate it for us later.


```
2020-10-01      Example simple sale
        Income:Sale             -120
        Assets   
```


### Automated transactions
If you have something you want to have happen to your money every time a certain transaction occurs, then you can specify a 'rule', or an automated transaction.

For instance, if you want 25% of all your income to go towards a special savings account.
```
=/^Income.*/
        [Income]                -1
        [Assets:Savings]        0.25
        [Assets:Checking]       0.75

```
[^1]
[^1]: The first line with the equality sign uses regex syntax to specify the string pattern to look for, for our new rule to apply to. See [examples](https://devhints.io/regexp)


## Using your ledger to generate reports
With ledger-cli there are 3 different types of reports we can generate:
- balance report
- register report
- cleared report

If your ledger file is called `my.ledger` for instance, then this command will generate the current balance:
```
$ ledger -f my.ledger balance

                   0  Assets
                  90    Checking
                  30    Savings
                 120  Income:Sale
--------------------
                   0

```


# Handling payments

## Payments from world to Denmark - comparison
Assuming we have a business in Denmark and would like to accept international orders, what are the options available and how do they compare with respect to cost of fees, and what else might be relevant to take into account?

Based on the table below, I would recommend starting with [TransferWise](https://transferwise.com/invite/spu/paulh2606) for ease of getting started quickly, but in a long term perspective I think [Stellar](https://stellar.org) will have more potential.

Here is a brief outline of 3 options:
| Service | Fees | Comments |
| ------ | ------ | --- |
| [PayPal](https://www.paypal.com/dk/webapps/mpp/paypal-fees) | High fees for currency conversion. <br> Generally complicated fee structure [depending on countries involved](https://www.finder.com/uk/paypals-fees-explained). | Founded in 1998 <br> Last I tried (2018) it was not available in Africa, but [now it seems that they have started](https://www.paypal.com/us/webapps/mpp/country-worldwide). |
| [TransferWise](https://transferwise.com/us/multi-currency-account/pricing) | Most transactions are free <br> Currency conversion fee is as little as 4kr | Founded in 2010. <br> Free account that lets you receive EUR, USD, GBP, PLN, AUD, NZD, SGD, HUF & RON also for free. <br> Allows the use of ['jars'](https://transferwise.com/help/articles/2978074/what-are-jars-and-how-do-i-use-them) for practice saving for a future goal. <br> Get a Mastercard with your account to allow easy spending. |
| [Stellar XLM](https://stellar.org/learn/stellar-for-remittances) | 0.000001 XLM (0.00005 DKK) fee when sending | Founded in 2014 <br> Stellar is a blockchain-based decentralized platform for transactions. <br> Available to everyone without any need for registration (when you 'create an account' on Stellar you get a **private key** and this is the one that lets you control your account no matter where you are) <br> Can be used with any [Stellar wallet](https://www.stellar.org/lumens/wallets) anywhere in the world. I would recommend creating a stellar wallet on [Keybase](https://book.keybase.io/wallet). <br> To spend money received on Stellar wallet, it must either be converted and transfered to your normal bank account, or try a 3rd party service like [bitpay](https://bitpay.com/card/) that allows spending cryptocurrencies directly via a debit card. <br> The value of Stellar and other cryptocurrencies **[changes a lot!](https://www.tradingview.com/symbols/XLMUSD/)**, there can be high risk of losing value from month to month. |

### More about Stellar payment network
[Blockchain](https://www.investopedia.com/terms/b/blockchain.asp) is a technology that is still rather new ([1](https://www.youtube.com/watch?v=SSo_EIwHSd4),[2](https://www.youtube.com/watch?v=RplnSVTzvnU),[3](https://www.youtube.com/watch?v=i9nUMvpT2rM)). People are still finding uses for it. To learn more about using Stellar, this [forum](https://galactictalk.org/) could be a place to start. 
Here are some other 3rd party solutions that might be useful: <br>
**Regarding being able to spend cryptocurrencies from a creditcard**
- https://bitpay.com/card/
- https://www.cryptowisser.com/debit-cards/
- https://crypto.com/en/cards.html
- https://xcard.io/

**Regarding converting cryptocurrencies to EUR or DKK and transfer to normal bank account**
- https://www.stellar.org/lumens/exchanges
- https://support.kraken.com/hc/en-us/articles/360000672763-How-to-withdraw-cryptocurrencies-from-your-Kraken-account
- https://www.piixpay.com

# Inspiration

## Regarding personal money management
The concept of the '5 jars' method of managing money was first popularized in the book [Craig Hill: 5 secrets of wealth that 96% of us dont know](https://www.goodreads.com/book/show/15760949-five-wealth-secrets-96-of-us-don-t-know). It takes it's knowledge from the bible and other stories of the author, and presents us with idea of 'putting first things first' with regards to our incomes. The five jars are:
1. Tithing: Money set aside to give to God, typically through religious organisation or other spiritual relationship.
1. Offering/Charity: Money set aside so you can help others in need financially.
1. Savings: Money set aside for unexpected 'rainy days' or for big vacations or a new house.
1. Investments: Money set aside for investments that will bring an 'automatic' percentage increase over time. It could be lending money out at an interest rate, using a savings account that have a rate, or buying a business that provides a continual income. The basic idea with this jar is that value should never drop below the (inflation-adjusted) amount of money that we have put into it.
1. Spending: Money set aside to pay for your daily necessary expenses.


- https://propertyupdate.com.au/the-5-jar-method/
- http://6jars.com/
- https://www.daveramsey.com/blog/envelope-system-explained
- [Chasing Purpose, Not Money](https://patricewashington.com/)([2](https://www.inc.com/andrew-thomas/3-big-reasons-why-purpose-leads-to-profits.html), [3](http://coachingwithchristina.com/my-nifty-money-management-system/))


# Calculating profit and contribution ratio of products
Being in business is usually more fun when there is a profit. Keeping track of how your profit came, makes you better able to steer the future direction of the business. In short there are at least three important financial parameters that influence your profits: 1) sales price of products, 2) variable costs of products, 3) fixed costs of your business operation.
The formula for calculating [net profit](https://www.investopedia.com/terms/n/net_margin.asp) is:
```math
profit_{total} = income_{total} - expenses_{total}
```

*net profit* is also called [*Avance*](https://www.amino.dk/ordbog/hvad-er-avance.aspx) in Danish.

Where profits are typically calculated on the business as a whole, the [contribution ratio](https://www.investopedia.com/terms/c/contributionmargin.asp) is used to calculate the *contribution* of individual products towards the profit of the company. Once we know this, then we are able to find out which products are most profitable to the business. This contribution is calculated as the ratio between the profit of a product (excluding [fixed costs](https://www.investopedia.com/ask/answers/032515/what-difference-between-variable-cost-and-fixed-cost-economics.asp)) in relation to it's sales price:
```math
contribution ratio = \cfrac{profit_{product}}{salesprice} = \cfrac{sales price - production cost}{sales price}
```

*contribution ratio* is also called [*Dækningsgrad*](https://www.amino.dk/ordbog/hvad-er-daekningsbidrag.aspx) in Danish.

For reference, it is not uncommon to see cafe's and restaurants operating with a contribution ratio of around $`0.6`$ or higher. The high ratio also allows them to cover the high fixed costs of these types of businesses.
